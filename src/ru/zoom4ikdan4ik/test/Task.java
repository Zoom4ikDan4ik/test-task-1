package ru.zoom4ikdan4ik.test;

import ru.zoom4ikdan4ik.test.streams.ReaderConsole;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Task {
    private static final ReaderConsole reader = new ReaderConsole();

    private static long X, Y;
    private static long x, y;

    public static void main(String[] args) throws IOException {
        List<Boolean> bugs = new ArrayList<>();

        X = reader.readLong();
        Y = reader.readInt();

        x = X;
        y = Y;

        for (long xi = 0; xi < x; xi++)
            bugs.add(false);

        if (Y > 1) {
            bugs = generateFirstBlock(bugs);
            x /= 2;
            bugs = generateSecondBlock(bugs);

            printLast(bugs);
        } else if (Y == 1)
            if (x % 2 == 0)
                System.out.println((x / 2 - 1) + "," + (x / 2));
            else System.out.println((x / 2) + "," + (x / 2));
    }

    private static List<Boolean> generateFirstBlock(List<Boolean> bugs) {
        long start = x - 1;
        final long distance = getDistance(x, y, false);
        long temp_distance = distance;

        for (long xi = start; xi > start / 2; xi--) {
            temp_distance -= 1;

            if (temp_distance == 0 && y > 0) {
                bugs.set((int) xi, true);

                temp_distance = distance;
                y -= 1;
            }
        }

        return bugs;
    }

    private static List<Boolean> generateSecondBlock(List<Boolean> bugs) {
        long start = x;
        final long distance = getDistance(x, y, true);
        long temp_distance = distance;

        for (long xi = start; xi > 0; xi--) {
            temp_distance -= 1;

            if (temp_distance == 0 && y > 0) {
                bugs.set((int) xi, true);

                temp_distance = distance;
                y -= 1;
            }
        }

        return bugs;
    }

    private static long getDistance(long x, long y, boolean flag) {
        return Math.round((double) x / y);
    }

    private static void printLast(List<Boolean> bugs) {
        boolean found = false;
        int left = 0;
        int right = 0;

        for (int index = 0; index < bugs.size(); index++)
            if (bugs.get(index))
                if (found) {
                    System.out.println(left + "," + right);

                    break;
                } else
                    found = true;
            else if (!found)
                left += 1;
            else right += 1;
    }
}
