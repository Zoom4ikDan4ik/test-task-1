package ru.zoom4ikdan4ik.test.streams;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class ReaderConsole {
    private BufferedReader bufferedReader;

    public ReaderConsole() {
        this.bufferedReader = new BufferedReader(new InputStreamReader(System.in));
    }

    public int readInt() throws IOException {
        return Integer.valueOf(this.bufferedReader.readLine());
    }

    public long readLong() throws IOException {
        return Long.valueOf(this.bufferedReader.readLine());
    }
}
